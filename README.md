# README #

This is a test project that computes the price of a shopping basket. I spent 2 hours on it, in which I set up the development environment and I used TDD to do a basic implementation. Because of the time constraints, some features are missing, namely:

* better error handling - like rules about negative counts and better messages about what went wrong
* better presentation - like a website or mobile or desktop GUI
* depending on the requirements, the product prices and pricing rules would be stored in some kind of database 
* tests that handle more cases

### How do I get set up? ###

* Clone or download the code to you local machine
```sh
git clone https://bitbucket.org/manisiu/solera.git
```
* To run it, from the solera folder, run npm install to install all dependencies, then run npm start to run the program
```sh
npm install
npm start
```
* To change the content of the basket, modify the input.txt file
* Run the tests with npm test
```sh
npm test
```