var readline = require('readline');
var fs = require('fs');
var Cart = require('./src/cart');
var cart = new Cart();
var calculator = require('./src/calculator');
var reporter = require('./src/reporter');

var rl = readline.createInterface({
    input: fs.createReadStream('./input.txt'),
    output: process.stdout,
    terminal: false
});

rl.on('line', function (line) {
    var args = line.split(/\s+/);
    if (args.length === 2) {
        cart.add(args[0], Number(args[1]));
    } else {
        console.log('Invalid input file');
        process.exit(1);
    }
});

rl.on('close', function (line) {
    calculator(cart, function (err, results) {
        if (err != null) {
            return console.log("error calculating the value of the basket");
        }
        console.log(reporter(results.items, results.discounts, results.total));
    });
});