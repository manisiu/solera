function Cart() {
    this.list = [];
}

Cart.prototype.add = function (product, count = 1) {
    var index = this.list.findIndex(x => x.product === product);
    if (index === -1) {
        this.list.push({ product: product, count: count });
    } else {
        this.list[index].count += count;
    }
};

Cart.prototype.getItems = function (callback) {
    var items = [];
    try {
        this.list.forEach(function (item) {
            items.push({ product: item.product, count: item.count });
        });

        items.getItem = function (product) {
            var index = this.findIndex(x => x.product === product);
            if (index === -1) {
                return 0;
            }
            return this[index];
        };
        callback(null, items);
    }
    catch (err) {
        callback(err);
    }

};

Cart.prototype.clear = function () {
    this.list.length = 0;
};



module.exports = Cart;