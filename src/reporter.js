var sprintf = require('sprintf-js').sprintf;
var eol = require('os').EOL;

function reporter(itemPrices, itemDiscounts, total) {
    var receiptString = sprintf('%-20s%10s%10s', 'item', 'count', 'price') + eol;
    itemPrices.forEach(function (item) {
        receiptString += sprintf('%-20s%10d%10f', item.product, item.count, item.price) + eol;

    });

    if (itemDiscounts.length > 0) {
        receiptString += getDividerLine() + eol;
        receiptString += sprintf('%-30s%10s', 'item', 'discount') + eol;
        itemDiscounts.forEach(function (item) {
            receiptString += sprintf('%-30s%10f', item.product, item.price) + eol;
        });
    }
    receiptString += getDividerLine() + eol;
    receiptString += sprintf('%40s', 'total') + eol;
    receiptString += sprintf('%40f', total) + eol;
    receiptString += getDividerLine() + eol;
    return receiptString;
}

function getDividerLine() {
    return '='.repeat(40);
}

module.exports = reporter;