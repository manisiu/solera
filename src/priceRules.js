var Discount = require('./discount');
exports.prices =
    {
        'apple': 0.25,
        'orange': 0.30,
        'garlic': 0.15,
        'papaya': 0.50,
    };

exports.discountedProducts = {};
exports.discountedProducts['papaya'] = new Discount(3, 1);


