var prices = require('./priceRules').prices;
var discountedProducts = require('./priceRules').discountedProducts;

function calculate(cart, callback) {
    cart.getItems(function (err, items) {
        if (err !== null) {
            return callback(new Error('Error retrieving the items. Please check the input file'));
        }
        var discounts = [];
        var total = 0;

        items.forEach(function (item) {
            var price = prices[item.product];
            if (price !== undefined) {
                item.price = item.count * price;
                var discountedPrice = 0;
                if (discountedProducts[item.product] !== undefined) {
                    discountedPrice = discountedProducts[item.product].getDiscountedItems(item.count) * price;
                    discounts.push({ product: item.product, price: discountedPrice });
                }

                total = total + item.price - discountedPrice;
            }
        });

        callback(null, { items: items, discounts: discounts, total: total });
    });
}

module.exports = calculate;