function Discount(total, free) {
    this.total = total;
    this.free = free;
}

Discount.prototype.getDiscountedItems = function (itemCount) {
    var discountedCount = Math.trunc(itemCount / this.total) * this.free;
    return discountedCount;
};

module.exports = Discount;