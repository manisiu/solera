var assert = require('assert');
calculator = require('../src/calculator');
var cart = new (require('../src/cart'))();

describe('calculator test', function () {
    beforeEach(function () {
        cart.clear();
    });

    it('should correctly calculate regular items', function (done) {
        cart.add('apple', 4);
        calculator(cart, function (err, results) {
            assert.equal(results.items.getItem('apple').price, 1);
            done();
        });
    });

    it('should correctly calculate total of regular items', function (done) {
        cart.add('apple', 4);
        cart.add('garlic', 3);
        calculator(cart, function (err, results) {
            assert.equal(results.total, 1.45);
            done();
        });
    });

    it('should correctly apply discounts to special items', function (done) {
        cart.add('papaya', 8);
        calculator(cart, function (err, results) {
            assert.equal(results.discounts[0].price, 1);
            done();
        });
    });

    it('should correctly apply total to when both special and regular items are present', function (done) {
        cart.add('papaya', 4);
        cart.add('apple', 3);
        calculator(cart, function (err, results) {
            assert.equal(results.total, 2.25);
            done();
        });
    });
})