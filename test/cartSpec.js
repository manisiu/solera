var assert = require('assert');


var cart = new (require('../src/cart'))();

describe('cart suite', function () {

    beforeEach(function () {
        cart.clear();
    });

    it('should present single added item to cart as 1 item', function (done) {
        cart.add('banana', 1);
        cart.getItems(function (err, items) {
            assert.equal(items.length, 1);
            done();
        });
    })

    it('should present added item with multiple count to cart as 1 item', function (done) {
        cart.add('large potato', 12);
        cart.getItems(function (err, items) {
            assert.equal(items.length, 1);
            done();
        });
    })

    it('should present added items multiple times to cart as 1 item', function (done) {
        cart.add('banana', 2);
        cart.add('banana', 3);
        cart.getItems(function (err, items) {
            assert.equal(items.length, 1);
            done();
        });
    })

    it('should add counts of items with same name', function (done) {
        cart.add('banana', 2);
        cart.add('apple', 1);
        cart.add('banana', 4);
        cart.add('apple', 4);
        cart.getItems(function (err, items) {
            assert.equal(items.getItem('banana').count, 6);
            assert.equal(items.getItem('apple').count, 5);
            done();
        });
    })
})